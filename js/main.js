const api_url = "https://www.vam.ac.uk/api/json/museumobject/";
const image_url = "http://media.vam.ac.uk/media/thira/collection_images/";
const image_styles = ["", "_jpg_s", "_jpg_o", "_jpg_ws", "_jpg_w", "_jpg_ds", "_jpg_l"];

var current_offset = 0;
var places = [];
var objects = [];

// Store the previous objects into the array so that the object can be loaded
var current_object_code = '';
var previous_objects = [];

/**
* This methid will format the typical array returned by the API for collections, categories and other data
*/
function format_array_to_string(array) {
  // Create a counter for the value
  counter = 0;
  // Create the variable where the value will be stored
  string_value = "";
  // Loop over the key data
  array.forEach(function(data_value){
    if (counter > 0) {
      // Add the name to the string name with a leading comma
      string_value += ", " + data_value.fields.name;
    } else {
      // Add the name to the string name
      string_value += data_value.fields.name;
    }

    // Increment the counter value
    counter++;
  })

  // Return the string value
  return string_value;
}

/**
* This function will handle the flash message that will tell the user what has happened
*/
function handle_banner_text(object, icon_class, text) {
  // Show the success message
  banner = object.closest(".show_object").querySelector('.message_banner')
  banner.innerHTML = '<i class="' + icon_class + '"></i> ' + text
  banner.classList.add("show")

  // Hide the banner after a fe seconds
  setTimeout(function(){
    banner.classList.remove("show");
  }, 5000)
}

function accepted_cookeis() {
  // Check to see if a local storage item has the cookies key and the value of yes
  return localStorage.getItem("accepted_cookies") == "yes";
}

// Function to handle the user accepting cookies
function accept_cookies(e) {
  e.preventDefault();
  // Set a cookies item with the cookies key and the value of yes
  localStorage.setItem("accepted_cookies", "yes");
  // Hide the modal
  document.querySelector("BODY").classList.remove('show_cookies');
}

// Function to decline cookies. This could be changed at anytime
function decline_cookies(e) {
  e.preventDefault();
  // Clear all of the stored items
  localStorage.clear();
  // Hide the modal
  document.querySelector("BODY").classList.remove('show_cookies');
}

/**
* This method will toggle the menu on and off for mobile devices
*/
function toggle_menu_classes() {
  // Toggle the show class for the NAV and the FOOTER
  document.querySelector("NAV").classList.toggle("show");
  document.querySelector("FOOTER").classList.toggle("show");
}

/***
* This method will handle the click event for opening and closing the menu
*/
function toggle_menu(e){
  e.preventDefault();
  // Call the method that will toggle the menu
  toggle_menu_classes();
}

// Return true or false depending on the value
function check_for_present_value(value){
  return (value !== undefined && value != "" && value.length != 0)
}

function image_path_gen(image_id, format=0){
  if (check_for_present_value(image_id)){
    return image_url + image_id.slice(0, 6) + "/" + image_id + image_styles[format] + ".jpg";
  }
  return "https://place-hold.it/300x300?text=No%20Image%20Available&italic=true"
}

// This function will handle the response when the they have no title set in the object
function handle_title(object){
  // If no title has been set select a new piece of information
  if (object.title === ""){
    // For now return "Unknown"
    return "Unknown";
  }
  // Return the title that will be used
  return object.title
}

// function to fidn the correct description
function handle_description(object){
  // Check for the historical_context_note first
  if (check_for_present_value(object.historical_context_note)){
    return object.historical_context_note;
  } else if (check_for_present_value(object.history_note)){
    // If the history note is available that will do
    return object.history_note;
  } else if (check_for_present_value(object.descriptive_line)){
    // If the descriptive line is available that will do
    return object.descriptive_line;
  }

  return "No Description Supplied"
}


// A fucntion to create the HTML that is used for each grid item
function build_grid_object(object){
  var object_data = object.fields;
  var object_number = object_data.object_number;
  var grid_item = "<a href=\"#\" data-object-code=\"" + object_number + "\" data-place=\"" + object_data.place + "\" data-object=\"" + object_data.object + "\">";
  image_path = image_path_gen(object_data.primary_image_id, 3);
  object_title = handle_title(object_data);
  grid_item += "<img src=\"" + image_path + "\" alt=\"" + object_title + "\"/>";
  grid_item += "<section>" + object_title + "</section>"
  grid_item += "</a>";

  return grid_item;
}

function load_objects(offset, random="", limit=50){
  var request = api_url + "search?limit=" + limit;
  if(offset != 0){
    request += "&offset=" + offset;
  }
  if(random === "" || random === true){
    request += "&random=1"
  }

  var send_request = new XMLHttpRequest();

  send_request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      var target = document.querySelector(".grid_view > DIV");

      json_response = JSON.parse(this.responseText);
      json_response.records.forEach(function(object){
        target.innerHTML += build_grid_object(object)
      });

      // Add the event listners to allow them to be clicked
      add_show_event_listener();

      // If the height is less thans the total height load the next lot ready
      if (current_offset === 0){
        current_offset += 50 // Set the offset to be 50 more than its current value
        load_objects(current_offset, true); // Call the method again to add more values
      }
    }
  }

  send_request.open("GET", request, true); // Prepare the request to be sent
  send_request.send(); // Send the request


  // Send the request and handle the response to add them to the
  // main
}

// Method that will be used to track where the user has scrolled to
function handle_scroll(e){
  element = document.querySelector("ARTICLE.grid_view")
  actual_height = document.querySelector("ARTICLE.grid_view DIV")
  scroll_height = element.scrollTop + element.offsetHeight
  total_height = actual_height.offsetHeight - 100

  if(scroll_height >= total_height){
    current_offset += 50 // Increment the current offset by 50 to get the next lot of objects
    // Load the next lot of objects into the grid_list
    load_objects(current_offset, true);
  }
  // Show the back to top button when they scroll down the height of the page
  back_to_top_button = document.querySelector('.back_to_top');
  if (element.scrollTop >= (element.offsetHeight * 2)){
    back_to_top_button.classList.add("show"); // Add the class of show
  } else {
    back_to_top_button.classList.remove("show"); // Remove the show class
  }
}

// Method to return the objects to the related items Method
function send_related_results_request(request, target, current_object){
  var new_request = new XMLHttpRequest();

  new_request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      // get the response items and convert to JSON
      json_response = JSON.parse(this.responseText);
      // Loop over the items that have been returned and add them to the related items
      json_response.records.forEach(function(item){
        // Make it easier to get the infomation for the object
        object_details = item.fields;
        // Attempt to find the object in the related items
        found_objects = target.querySelectorAll("*[data-object-code=" + object_details.object_number + "]")
        // Only show them if they do not have the same object number and have not been duplicated
        if(object_details.object_number !== current_object && found_objects.length === 0){
          // Append the HTML for the related item by getting the image for them
          target.innerHTML += "<a href=\"#\" data-object-code=\"" + object_details.object_number + "\"><img src=\"" + image_path_gen(object_details.primary_image_id, 2) +  "\" alt=\"" + object_details.title + "\"/></a>";
        }
      })
    } else {
      return []; // return a blank array if it fails to send
    }
    // Add all the event listeners again
    add_show_event_listener();
  }

  new_request.open("GET", request, true);
  new_request.send();
}

/**
* This function will split a string so it is ready for the API request
*/
function prepare_string_for_api(string) {
  // Create that variable that will be used
  split_string = [];

  // It it contains a comman they will need to be handled
  if(string.includes(",")){
    // Spit the string on the comma
    split_string = string.split(",")
  } else {
    // Otherwise handle the spaces
    split_string = string.split(" ")
  }

  // Trim each of the values in the array to remove all leading and trailing spaces
  split_string.forEach(function(string_value, index){
    // Set the value to be itself trimmed
    split_string[index] = string_value.trim();
  });

  // Join the array using a plus symbol
  prepared_string = split_string.join("+");
  // Swap all spaces for a plus symbol
  prepared_string = prepared_string.replace(" ", "+");

  // Send the prepared sting back to the where it was called
  return prepared_string;
}

// Method to get all of the related items
function get_related_items(object, target){
  // Store all the related items into the hash that will be returned
  if (object.names.length == 0){
    return '';
  }

  // Get the value of artists prepared as a string
  artist_name = prepare_string_for_api(object.names[0].fields.name);
  // Prepare the materials for the search
  materials_string = prepare_string_for_api(object.materials_techniques);
  // Prepare the place to be used
  place_string = prepare_string_for_api(object.place);
  // Prepare the categories for search
  categories_string = prepare_string_for_api(format_array_to_string(object.categories));
  // Prepare the collections for API query
  collection_string = prepare_string_for_api(format_array_to_string(object.collections));

  // Get the related items section
  related_target = target.querySelector('SECTION.related_items DIV.items');
  // Clear the targte ready to be populated
  related_target.innerHTML = "";

  send_related_results_request(api_url + '/search?namesearch=' + artist_name + '&limit=10', related_target, object.object_number);
  send_related_results_request(api_url + '/search?materialsearch=' + materials_string + '&limit=10', related_target, object.object_number);
  send_related_results_request(api_url + '/search?placesearch=' + place_string + '&limit=10', related_target, object.object_number);
  send_related_results_request(api_url + '/search?q=' + categories_string + '&limit=10', related_target, object.object_number);
  send_related_results_request(api_url + '/search?q=' + collection_string + '&limit=10', related_target, object.object_number);

}

// Added a function to add the event listeners to the grid item
function add_show_event_listener(){
  // Find all the elements that have the object code set
  links = document.querySelectorAll("*[data-object-code]")
  // Make all of them clickable and add the event listener to open the panel
  links.forEach(function(link){
    // Remove it if the event listener has already been added
    link.removeEventListener('click', show_item_page)
    // Add the even listener
    link.addEventListener('click', show_item_page)
  })
}

// This function will populate the data list for the object
function populate_data_list(target, object_data) {
  // Create a variable to write the HTML in one hit
  built_html = "";
  // This variable will be the keys that the application will look for
  data_keys = [
    {
      key: 'artist',
      label: 'Artists'
    }, {
      key: 'categories',
      label: 'Categories'
    }, {
      key: 'collections',
      label: 'Collections'
    }, {
      key: 'dimensions',
      label: 'Dimensions'
    }, {
      key: 'location',
      label: 'Current Location'
    }, {
      key: 'materials_techniques',
      label: 'Materials & Techniques'
    }, {
      key: 'place',
      label: 'Place'
    }, {
      key: 'credit',
      label: 'Credit'
    }
  ];

  // Loop over all the keys
  data_keys.forEach(function(key_object){
    // Attempt to get the data for the object
    key_data = object_data[key_object.key]
    // If the data is available then the HTML can be added
    if (key_data !== undefined) {

      value = key_data
      // Now create the data for the data list
      if (Array.isArray(key_data) && key_data.length > 0) {
        // Add the string value to the built HTML
        value = format_array_to_string(key_data);
      }

      // Check that the data has a value
      if (value !== undefined && value.length > 0) {
        // Create the title for the data
        built_html += '<dt>' + key_object.label + '</dt>';
        // Create the value
        built_html += "<dd>" + value + "</dd>"
      }
    }
  })

  // Append the data list to the target
  target.innerHTML = built_html;
}

/**
* This function will handle the request for the object and populate the show panel
*/
function complete_show_object(code, back_mode) {
  var request = new XMLHttpRequest();

  // Set the current object to be the current item
  current_object_code = code;

  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      // Convert the response into JSON so it can be used
      json_response = JSON.parse(this.responseText);
      object = json_response[0].fields

      // Set the object title so that it is handled
      object_title = handle_title(object);

      // Look to get the back object if the mode is true and
      // slide it in from the other direction
      target = document.querySelector('ARTICLE.show_object:not(.current)')
      if (back_mode == true){
        target.classList.add('back_object')
      }


      // Set the title in the show object
      target.querySelector("H1").innerHTML = object_title;
      // Set the image at the top of the display
      image = target.querySelector(".image IMG")
      image.setAttribute("src", image_path_gen(object.primary_image_id, 6));
      image.setAttribute("alt", object_title);
      // Set the description
      target.querySelector('.description').innerHTML = handle_description(object);
      // Add the images to the gallery
      gallery_section = target.querySelector(".gallery .images")
      gallery_section.innerHTML = "";
      object.image_set.forEach(function(image){
        image_fields = image.fields;
        // Append each item to the gallery
        gallery_section.innerHTML += "<a href=\"#\" aria-label=\"Open Gallery Image\"><img src=\"" + image_path_gen(image_fields.image_id, 2) +  "\" alt=\"\" data-image-code=\"" + image_fields.image_id + "\"/></a>";
      });

      // Populate the data list with the data from the API
      data_list = target.querySelector(".data DL")
      // Clear the data list
      data_list.innerHTML = "";
      // Use the object to populate the datalist
      populate_data_list(data_list, object);

      // Set the share link by first getting the input
      input = target.querySelector(".share_link");
      input.value = "http://" + window.location.host + "/?object_number=" + code
      input.classList.remove("show")

      // Remove current from all of the panels
      document.querySelectorAll('.current').forEach(function(panel){
        panel.classList.remove('current')
      });

      // Get the items that are related to the current object
      get_related_items(object, target);

      target.querySelector(".save_current_item").classList.remove("hide")
      // Hide the save button if they are already stored
      if(is_item_stored(code)){
        // Hide the save button in the object
        target.querySelector(".save_current_item").classList.add("hide")
      }

      // Add current to the new panel, this will animate the change
      target.classList.add('current')
      // Remove the back_object class
      target.classList.remove('back_object')
      // Ensure the body has the show object class
      document.querySelector("BODY").classList.add('show_object');
    }
  }

  // Set the url ready to send and get the result for the object they want to see
  request.open("GET", api_url + code, true);
  request.send(); // Send the request
}

// A function to populate the show item and show the panel
function show_item_page(e){
  e.preventDefault(); // Stop the element from performing its default action

  // Set the variable that will be used throughout this action
  var back_mode = false;
  var code = this.dataset.objectCode

  /**
   * Find if the back button has been clicked by looking at the class names
   * contains the class for a back button
  */
  if (this.classList.contains('back_button')){
    back_mode = true; // Set the back mode to be true
    code = previous_objects.pop(); // Get the last previous item and remove it from the list
    // If there are not more items in the list then close the panel
    if (previous_objects.length == 0){
      // Remove the class from the body element
      document.querySelector("BODY").classList.remove('show_object');
      // Dont allow the function to run any longer
      return;
    }
  }

  if(back_mode == false && current_object_code !== '') {
    /**
     * If it is not a back mode and the current code is not blank
     * add the old code into the previous codes array
    */
    previous_objects.push(current_object_code)
  }

  complete_show_object(code, back_mode);
}

// Function to hide the show object window
function hide_show_object(e){
  e.preventDefault();
  // Remove the class that shows the article panel
  document.querySelector("BODY").classList.remove('show_object');
}

/**
 * This fucntion will handle the click event to open the gallery items
 */
function open_gallery(e){
  // Stop the element from performing its default action
  e.preventDefault();
  if (e.target.tagName == "IMG"){
    // Get the current image code
    var current_image_code = e.target.dataset.imageCode
    var added_current_code = false;
    var all_images = [];
    var image_target = document.querySelector("ARTICLE.gallery_show .images")

    // Look at the gallery and populate it with all of the images
    e.target.parentNode.parentNode.querySelectorAll("A").forEach(function(image){
      all_images.push(image.querySelector("IMG").dataset.imageCode)
    });

    image_target.innerHTML = "";
    class_name = "prev";
    all_images.forEach(function(image){

      if(image == current_image_code){
        // Set the class_name variable to a blank string if
        class_name = "";
        // Register that the current code has been added
        added_current_code = true;
      } else if (added_current_code){
        // if the current code has been added then set the class
        // to be next
        class_name = "next"
      }
      image_html = "<img src=\"" + image_path_gen(image, 0) + "\" class=\"" + class_name + "\">"
      image_target.innerHTML += image_html;
    });

    // Add the images to the gallery box
    // Show the gallery by changing the class on the body if images are available
    if (all_images.length > 0){
      document.querySelector("BODY").classList.add("show_gallery");
    }
  }

  // Handle the controls of the gallery
  handle_gallery_commands();
}

/**
 * This function will handle the closeing of the gallery
 * it will remove the class of show gallery from the body
 */
function close_gallery(e){
  // Stop the element from completing its usual task
  e.preventDefault();
  // Remove the "show_gallery" class from the body
  document.querySelector("BODY").classList.remove("show_gallery");
}

/**
 * This function will handle the on click event for the next
 * gallery item
 */

function next_gallery_item(e){
  // Stop the default action
  e.preventDefault();
  var gallery_element = document.querySelector("ARTICLE.gallery_show")

  // Check if the button has been disabled
  if (check_button_is_disabled(this)){
    // If the button have been disabled stop the function
    return;
  }

  // Get the current image
  current_image = gallery_element.querySelector(".images IMG:not(.next):not(.prev)");
  // Get the next image
  next_image = gallery_element.querySelector(".images IMG.next");

  // Give the current item the prev class and remove the next class from the next item
  current_image.classList.add("prev");
  next_image.classList.remove("next");

  // Handle the controls again to disable buttons that are not needed
  handle_gallery_commands();
}

/**
 * This function will handle the on click event for the previous
 * gallery item
 */

function prev_gallery_item(e){
  // Stop the default action
  e.preventDefault();

  // Check if the button has been disabled
  if (check_button_is_disabled(this)){
    // If the button have been disabled stop the function
    return;
  }

  var gallery_element = document.querySelector("ARTICLE.gallery_show")
  // Get the current image
  current_image = gallery_element.querySelector(".images IMG:not(.next):not(.prev)");
  // Get the previous image
  prev_images = gallery_element.querySelectorAll(".images IMG.prev");
  prev_image = prev_images[prev_images.length - 1];

  // Give the current item the next class and remove the prev class from the prev item
  current_image.classList.add("next");
  prev_image.classList.remove("prev");

  // Handle the controls again to disable buttons that are not needed
  handle_gallery_commands();
}

/**
 * Check to see if the button has the disable class
 * This will return true or false
 */
function check_button_is_disabled(button){
  // Return the value that will come from the contains function
  return button.classList.contains("disable")
}

/**
 * Function to adjust the gallery depending on the images in the gallery
 * This will turn the next and previous buttons on and off
 */
function handle_gallery_commands(){
  // Get the elements that will be used multiple times
  var gallery_element = document.querySelector("ARTICLE.gallery_show"); // Get the gallery
  var prev_button = gallery_element.querySelector('.prev_item'); // Get the previous button
  var next_button = gallery_element.querySelector('.next_item'); // Get the next button

  // If there are not images with the PREV class then hide the previous button
  if(gallery_element.querySelectorAll(".images IMG.prev").length < 1){
    // Add the disable class
    prev_button.classList.add("disable")
  } else {
    // remove the disable class when the gallery has previous images
    prev_button.classList.remove("disable")
  }

  // Check for any images with the NEXT class
  if(gallery_element.querySelectorAll(".images IMG.next").length < 1){
    // If none add the disable class
    next_button.classList.add("disable")
  } else {
    // Else remove the disable class to show the button
    next_button.classList.remove("disable")
  }
}

/**
 * This function will look at the local storage to see if an object is already in the list
 */
function is_item_stored(object_code){
  // Find the local storage item for the stored items
  stored_items = localStorage.getItem('stored_items');

  // if the stored items is null then we can assume it has not been stored
  if (stored_items == null){
    // Send back false
    return false;
  }

  // Split the string from stored items into an array on the comma
  stored_items = stored_items.split(",");

  // Return the value of the the includes methos looking for the object code
  return stored_items.includes(object_code);
}

/**
 * This function will be used to save the current item when the save current
 * item button has been clicked
 */
function save_current_item(e){
  // Prevent the default action from happening
  e.preventDefault();

  // Check that the userchas accepted cookies
  if (accepted_cookeis() == false){
    // Stop the stored items from functioning
    return;
  }

  // Find the local storage item for the stored items
  stored_items = localStorage.getItem('stored_items');

  // if the stored items is null set the sorted items to be the current object code
  if (stored_items == null){
    stored_items = current_object_code;
  } else if(is_item_stored(current_object_code) == false) {
    // Else if the item is not already in the list add the current object to be list of items
    stored_items += "," + current_object_code;
  }

  // Set the localstorage item to be the stored items variable
  localStorage.setItem('stored_items', stored_items);

  // Hide the save button
  this.classList.add("hide")

  // Show the success message
  handle_banner_text(this, "far fa-check-circle", "Object saved")
}

/**
 *  This function will complete the request and load in each item as it is created
 */
function append_stored_item(item_code){
  // get the target for the item
  target = document.querySelector("MAIN > ARTICLE.saved_items > DIV")
  // Add a placeholder to make the loading look better
  target.innerHTML += "<a href=\"#\" data-object-code=\"" + item_code + "\" class=\"placeholder\"></a>";
  // Get the element that has just been created
  item_placeholder = target.querySelector("A.placeholder[data-object-code=" + item_code +"]")

  // Prepare the request that will be sent to get the element
  url = api_url + item_code;
  request = new XMLHttpRequest();

  // Handle the resonse for the request
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      json_response = JSON.parse(this.responseText);
      object_data = json_response[0].fields

      item_placeholder = target.querySelector("A.placeholder[data-object-code=" + object_data.object_number +"]")
      item_placeholder.innerHTML = "<section class=\"image\"><img src=\"" + image_path_gen(object_data.primary_image_id, 3) + "\"/>"
      item_placeholder.innerHTML += "<section class=\"info\"><h2>" + handle_title(object_data) + "</h2></section>"

      item_placeholder.classList.remove("placeholder")

      add_show_event_listener();
    }
  }

  // Send the request to the correct url
  request.open("GET", url, true);
  request.send();

}

/**
 * This fucntion will load in all of the saved items that the user has
 * saved once the view has been loaded
 */
function load_saved_items(){
  // First get all of the saved items
  saved_items = localStorage.getItem('stored_items')
  // Get the target element where the stored items are goning to be
  target = document.querySelector("MAIN > ARTICLE.saved_items > DIV")
  if (saved_items == null){
    // Show a message telling them they do not have any stored items
    target.innerHTML = "<h2>You have not saved any items.</h2>";
    return false;
  }

  // Cleat the stored item
  target.innerHTML = "";

  // If they have stored items they will need to be loaded in
  saved_items.split(",").forEach(function(item){
    append_stored_item(item);
  })
}

/**
* This function will close all the articles for them to then be re-opened
*/
function close_articles() {
  // Get all the articles in the main element
  document.querySelectorAll("MAIN > ARTICLE").forEach(function(article){
    // Add the hidden class for them all
    article.classList.add("hidden")
  });
}

/**
 * This fucntion will handle the nav items that the user will click on
 */
function handle_nav_items(e){
  // Stop the default action
  e.preventDefault();

  class_list = this.classList

  if (class_list.contains("saved_items")){
    close_articles()
    document.querySelector("MAIN > ARTICLE.saved_items").classList.remove("hidden")
    load_saved_items();
  } else if (class_list.contains("show_grid")) {
    close_articles()
    document.querySelector("MAIN > ARTICLE.grid_view").classList.remove("hidden")
  }

  /* Close the menu */
  toggle_menu_classes()
}

/**
* This function is the callback that can be used to clear all the saved items
*/

function clear_saved_items(e) {
  e.preventDefault(); // Stops the application completing the action

  // Remove all the stored items
  localStorage.removeItem("stored_items");

  // Set the stored items to be the none message
  target = document.querySelector("MAIN > ARTICLE.saved_items > DIV")
  target.innerHTML = "<h2>You have not saved any items.</h2>";
}

/***
* This function will handle the loading of a requested object
*/
function load_requested_object(){
  // Get the current url that has been requested
  current_url = window.location.href
  // Use regex to get the object number parameter
  captured = /object_number=([^&]+)/.exec(current_url);
  // Check that a value has been passed in
   if (captured !== null && captured.length > 1 && captured[1] !== undefined && captured[1].length > 0) {
     // Ensure all hashes have been removed
     code = captured[1].replace("#", "")
     // Show the object using the code requested
     complete_show_object(code, false)
   }
}

/**
* This function will handle the sharing of an object by copying a link to their clipboard
*/
function share_item(e) {
  // Don't allow it to complete the deafult action
  e.preventDefault();

  input = this.closest(".show_object").querySelector('.share_link')
  input.classList.add("show")

  // Select the value in the input
  input.select();
  // Copy text to clipboard
  document.execCommand('copy');

  // Show the success message
  // banner = this.closest(".show_object").querySelector('.message_banner')
  // banner.innerHTML = '<i class="far fa-check-circle"></i> Share link copied'
  // banner.classList.add("show")

  handle_banner_text(this, "far fa-check-circle", "Share link copied")
}

/**
* This function will set the scrollTop to be 0 on the grid view
*/
function scroll_to_top(e) {
  // Prevent the defalt action
  e.preventDefault();

  // Set the scrollTop value to zero
  document.querySelector('.grid_view').scrollTop = 0;
}

/**
 * This function is used to add the basic commands when the page loads
 */
function load_commands(e){
  // Add the event listener to the navigation controls
  document.querySelectorAll(".nav-toggle").forEach(function(toggle){
    toggle.addEventListener("click", toggle_menu);
  });

  // Load all of the initial objects into the grid container
  load_objects(0);

  // Add the scroll event listener
  document.querySelector("ARTICLE.grid_view").addEventListener('scroll', handle_scroll)

  // Add the event listener to go back to the grid view
  document.querySelector("ARTICLE.show_object .back_button").addEventListener("click", hide_show_object);

  // If the user has not accepted cookies then show the cookies message
  if(!accepted_cookeis()){
    // Add the show cookies class
    document.querySelector("BODY").classList.add('show_cookies');
  }

  /**
   * Add the event listeners to accept or decline cookies
   */
  document.querySelector(".accept_button").addEventListener("click", accept_cookies);
  document.querySelector(".decline_button").addEventListener("click", decline_cookies);

  /**
   * Add the event listener to all the back buttons
   */
  back_buttons = document.querySelectorAll('.back_button');

  /**
   * Loop over each back button and add the event listener to
   * the back button
   */
  back_buttons.forEach(function(button){
    button.addEventListener("click", show_item_page)
  });

  // Add callback to close the gallery
  var gallery = document.querySelector("ARTICLE.gallery_show")
  gallery.querySelector(".close_gallery").addEventListener("click", close_gallery)
  // Add the callback to open the gallery and load in the images
  document.querySelectorAll("ARTICLE.show_object SECTION.gallery .images").forEach(function(open){
    open.addEventListener("click", open_gallery);
  });
  // Add the callbacks to handle the next and previous image
  gallery.querySelector("A.next_item").addEventListener("click", next_gallery_item)
  gallery.querySelector("A.prev_item").addEventListener("click", prev_gallery_item)

  // Add the callback that will be used to add the current object to the saved items
  document.querySelectorAll(".save_current_item").forEach(function(button){
    button.addEventListener("click", save_current_item);
  });

  // Add the callback that will handle the nav items
  document.querySelectorAll("NAV UL A").forEach(function(item){
    item.addEventListener("click", handle_nav_items);
  });

  // Add the callback to handle clearing all the items
  document.querySelector(".clear_saved").addEventListener("click", clear_saved_items)

  // Load a requested object with should be in the params
  load_requested_object();

  //Handle the share link to show the link and copy it to the clipboard
  document.querySelectorAll(".share_item").forEach(function(share){
    share.addEventListener("click", share_item);
  });

  // Set the back to top button to scroll to the top of the grid
  document.querySelector(".back_to_top").addEventListener("click", scroll_to_top);
}

/**
 * When the document has loaded all the event listners will need to be added
 * to get all the actions to work and load in the items.
 */
document.addEventListener("DOMContentLoaded", load_commands);
