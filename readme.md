# CIS3159 - Collections API Task

This task asked for a generous interface that has been

## Installation

In order to get this project running on a local device

To install the appropriate packages run the command below

`npm i`

Compile your CSS from the SCSS files. This will also minify the CSS files to remove as much whitespace as possible. This will help to reduce d the amount the users will need to download.

`gulp watch`

To run the application you will need to navigate to the app directory by typing the commands below.

`cd app`

`php -S localhost:8000`

## Deploying

In order to deploy the application you will need to transfer the contents of the app directory to the sever after you have compiled the minified css files.

The first step is to ensure that the files are in their current state and are minified. To do this run the following command in the console, while with in the project directory.

`gulp compile`

Then you can copy the app directory into the server root directory.

## Design

## Features
