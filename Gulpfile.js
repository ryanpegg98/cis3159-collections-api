var gulp = require('gulp');
var sass = require('gulp-sass');
var clean = require('gulp-clean-css');
var minify = require('gulp-minify');

gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss') // Gets all files ending with .scss in a
    .pipe(sass())
    .pipe(gulp.dest('style'))// Send the files to the style directory so that they can be minified
})

gulp.task('watch', function(){
  gulp.watch('scss/**/*.scss', gulp.series('sass', 'minify-css'));// Compile the CSS files
  gulp.watch('js/**/*.js', gulp.series('minify-js')) // Compile the JS into a minified state
});

gulp.task('minify-css', function(){
  return gulp.src('style/*.css')
    .pipe(clean({debug: true}, (details) => {
      console.log(`${details.name}: ${details.stats.originalSize}`);
      console.log(`${details.name}: ${details.stats.minifiedSize}`);
    }))
  .pipe(gulp.dest('app/css'));
});

// Minify the JavaScript files to remove the whitespace
gulp.task('minify-js', function(){
  return gulp.src('js/**/*.js')
    .pipe(minify({
      ext: {
        min: '.min.js'
      },
      noSource: true
    }))
    .pipe(gulp.dest('app/js'))
});

gulp.task('compile', gulp.series('sass', 'minify-css', 'minify-js'));
